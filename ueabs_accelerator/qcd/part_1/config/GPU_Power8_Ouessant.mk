#NVIDIA GPU configuration file

MPIDIR=/opt/ibmhpc/pecurrent/mpich/gnu/
GPUS_PER_NODE=4

NVARCH=sm_60	
CFLAGS = $(DEFINES) -O2 -DARCH=0 -I $(MPIDIR)/include64 
LDFLAGS = -lm  -arch=$(NVARCH) -L./targetDP -ltarget -L$(MPIDIR)/lib64 -lmpi -lmpl -lm -lgomp
CC=mpcc -compiler gnu
TARGETCC=nvcc
TARGETCFLAGS=-x cu -arch=$(NVARCH) -I. -DCUDA -DVVL=1 -DSoA -DGPUSPN=$(GPUS_PER_NODE) -dc -c $(CFLAGS)

