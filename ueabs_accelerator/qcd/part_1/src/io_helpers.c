/********************** io_helpers.c **********************************/
/* MIMD version 6 */
/* DT 8/97 
   General purpose high level routines, to be used by any application
   that wants them.
 */

#include "./include/includes.h"

extern int totnodes[4];

/* reads lattice from the same lattice seen by any nodes */
void reload_lattice( int flag, char *filename )
{
    FILE *f;
    int x, y, z, t, ind, dir;
    double ssplaq, stplaq;
    double max_deviation;
    int a;
    su3_matrix_comp mat_comp;
    void coldlat(  );
    void ranlat(  );

    double mytime;
    mytime = -dclock(  );

    /* always use HOT lattice*/
    ranlat(  );
    
    mytime += dclock(  );
    g_sync_KE(  );

    d_plaquette( &ssplaq, &stplaq );
    node0_fprintf( file_o1, "reload_lattice: time= %.3g\t checkplaq: %e %e\n", mytime, ssplaq, stplaq );

    max_deviation = check_unitarity(  );
    g_doublemax_KE( &max_deviation );
    node0_fprintf( file_o1, "reload_lattice: Unitarity checked.  Max deviation %.2e\n", max_deviation );

}


    
void ranlat(  )
{
    /* sets link matrices to random SU(3) matrices */
    register int i, dir;
    register site *sit;
    void random_su3_KE( su3_matrix * r_su3, double eps );

    FORALLSITES( i, sit )
    {
	for ( dir = XUP; dir <= TUP; dir++ )
	{
	    random_su3_KE( &( gauge[4 * i + dir] ), 1.0 );
	}
    }
    node0_fprintf( file_o1, "ranlat: Random gauge configuration loaded\n" );
}


int get_f( FILE * f, char *variable_name_string, double * value )
{
    char readname[80];
    double read_d;
    char ch;
    int status;

    status = 0;
	rewind( f );
	do
	{
	    fscanf( f, "%s", readname );
	    if( strcmp( readname, variable_name_string ) == 0 )
	    {
		if( fscanf( f, "%lg", &read_d ) == 1 )
		{
		    *value = ( double ) read_d;
		    if( this_node == 0 )
			fprintf( file_o1, "get_f: %s\t %g\n", variable_name_string, *value );
		}
		else
		{
		    if( this_node == 0 )
			printf( "ERROR get_f: Error reading %s\n", variable_name_string );
		    status = 1;
		}
		goto endread;
	    }
	    else
	    {
		while( ( ch = getc( f ) ) != '\n' && ( !feof( f ) ) );
	    }
	}
	while( !feof( f ) );
	if( this_node == 0 )
	    printf( "ERROR get_f: Error reading %s\n", variable_name_string );
	status = 1;
  endread:

    return status;
}


int get_i_KE( FILE * f, char *variable_name_string, int *value )
{
    char readname[80];
    char ch;
    int status;

    status = 0;
	rewind( f );
	do
	{
	    fscanf( f, "%s", readname );
	    if( strcmp( readname, variable_name_string ) == 0 )
	    {
		if( fscanf( f, "%d", value ) == 1 )
		{
		    if( this_node == 0 )
			fprintf( file_o1, "get_i: %s\t %d\n", variable_name_string, *value );
		}
		else
		{
		    if( this_node == 0 )
			printf( "ERROR get_i: Error reading %s\n", variable_name_string );
		    status = 1;
		}
		goto endread;
	    }
	    else
	    {
		while( ( ch = getc( f ) ) != '\n' && ( !feof( f ) ) );
	    }
	}
	while( !feof( f ) );
	if( this_node == 0 )
	    printf( "ERROR get_i: Error reading %s\n", variable_name_string );
	status = 1;
  endread:

    return status;
}

/* get the string after variable until the end of line */
int get_s_KE( FILE * f, char *variable_name_string, char *value )
{
    char readname[256];
    char ch;
    int i, status;

    status = 0;
	rewind( f );
	do
	{
	    fscanf( f, "%s", readname );
	    if( strcmp( readname, variable_name_string ) == 0 )
	    {
		i = 0;
		while( ( ch = getc( f ) ) < 33 && ( !feof( f ) ) );
		do
		{
		    value[i] = ch;
		    i++;
		    ch = getc( f );
		}
		while( ch != '\n' && ( !feof( f ) ) );
		value[i] = 0;
		goto endread;
	    }
	    else
	    {
		while( ( ch = getc( f ) ) != '\n' && ( !feof( f ) ) );
	    }
	}
	while( !feof( f ) );
	if( this_node == 0 )
	    printf( "ERROR get_s: Error reading %s\n", variable_name_string );
	status = 1;
  endread:
    return status;
}

/* read the total number of nodes in each (physical) direction */ 
int get_totnodes( FILE * f, char *variable_name_string )
{
    char readname[80];
    char ch;
    int i, status;

    status = 0;
    rewind( f );
    do
    {
	fscanf( f, "%s", readname );
	if( strcmp( readname, variable_name_string ) == 0 )
	{
	    for ( i = 0; i < 4; i++ )
	    {
		if( fscanf( f, "%d ", &totnodes[i] ) != 1 )
		{
		    if( this_node == 0 )
			printf( "ERROR get_totnodes: Not enough directions\n" );
		    status = 1;
		}
		else
		{
		    if( this_node == 0 )
			printf( "get_totnodes: totnodes[%i]\t %i\n", i, totnodes[i] );
		}
	    }
	    goto endread;
	}
	else
	{
	    while( ( ch = getc( f ) ) != '\n' && ( !feof( f ) ) );
	}
    }
    while( !feof( f ) );
    if( this_node == 0 )
	printf( "ERROR get_totnodes: Error reading %s\n", variable_name_string );
    status = 1;
  endread:

    return status;
}
