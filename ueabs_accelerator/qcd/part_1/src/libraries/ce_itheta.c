/********************** ce_itheta.c (in complex.a) **********************/
/* MIMD version 6 */
/* Subroutines for operations on complex numbers */
/* exp( i*theta ) */
#include "../include/config.h"
#include <math.h>
#include "../include/complex.h"

complex ce_itheta_KE( double theta )
{
    complex c;
    c.real = ( double ) cos( ( double ) theta );
    c.imag = ( double ) sin( ( double ) theta );
    /* there must be a more efficient way */
    return ( c );
}
