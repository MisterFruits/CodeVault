MYFILES:=       \
	cadd.c  ce_itheta.c   cmplx.c cmul.c \
	  csub.c \
	 m_amatvec.c  \
	m_matvec.c  m_matvec_s.c \
	  realtr.c  \
	s_m_vec.c  \
	gaussrand.c  \
	m_su2_mat_vec_a.c m_su2_mat_vec_n.c   \
	 wp_grow.c wp_grow_a.c  clear_wvec.c \
	  g5_m_wvec.c

SOURCE+= $(patsubst %,libraries/%,$(MYFILES))

MYTARGETFILES:=   wvec_rdot.C s_m_a_wvec.C wp_shrink4.C m_amat_hwvec.C m_mat_hwvec.C grow4wvecs.C msq_wvec.C sub_wvec.C
 
TARGETSOURCE+= $(patsubst %,libraries/%,$(MYTARGETFILES))

