/******** make_lattice.c *********/

/* 1. Allocates space for the lattice fields, as specified by the
   application site structure.  Fills in coordinates, parity, index.
   2. Allocates gen_pt pointers for gather results 
   3. Initializes site-based random number generator, if specified
   by macro SITERAND */

#include "./include/includes.h"
void make_lattice(  )
{
    register int i, n;
    int x, y, z, t;

    /* allocate space for lattice, fill in parity, coordinates and index */
    MEMALIGN(lattice, site, sites_on_node);

    /* Allocate address vectors */
    for ( i = 0; i < N_POINTERS; i++ )
        MEMALIGN(gen_pt[i], char *, sites_on_node);

    for ( t = 0; t < nt; t++ )
        for ( z = 0; z < nz; z++ )
            for ( y = 0; y < ny; y++ )
                for ( x = 0; x < nx; x++ )
                {
                    if( node_number_KE( x, y, z, t ) == mynode_KE(  ) )
                    {
                        i = node_index_KE( x, y, z, t );
                        lattice[i].x = x;
                        lattice[i].y = y;
                        lattice[i].z = z;
                        lattice[i].t = t;
                        lattice[i].index = x + nx * ( y + ny * ( z + nz * t ) );
                        if( ( x + y + z + t ) % 2 == 0 )
                            lattice[i].parity = EVEN;
                        else
                            lattice[i].parity = ODD;
                    }
                }

    /* matrices */
    MEMALIGN(gauge, su3_matrix, 4*sites_on_node);
    MEMALIGN(gauge_32, float, 4*18*sites_on_node);
    for (i=0; i<8; i++){
        MEMALIGN(htmp[i],half_wilson_vector, sites_on_node);
        MEMALIGN(htmp_32[i],float, 12*sites_on_node);
    }    
    node0_fprintf( file_o1, "make_lattice: Mallocing %.1f Mbytes per node\n", ( double ) memsize / 1e6 );
}

/* returns i_mu */
int site_mu( int i, int mu )
{
    if( mu == 0 )
	return lattice[i].x;
    if( mu == 1 )
	return lattice[i].y;
    if( mu == 2 )
	return lattice[i].z;
    if( mu == 3 )
	return lattice[i].t;
    node0_printf( "ERROR site_mu: Out of direction range!\n" );
    terminate_KE( 0 );
    return -1;
}

/* taxi driver distance from the origin */
int taxi_dist( int j )
{
    int dist;
    dist = ( lattice[j].x <= nx / 2 ) ? ( lattice[j].x ) : ( nx - lattice[j].x );
    dist += ( lattice[j].y <= ny / 2 ) ? ( lattice[j].y ) : ( ny - lattice[j].y );
    dist += ( lattice[j].z <= nz / 2 ) ? ( lattice[j].z ) : ( nz - lattice[j].z );
    dist += ( lattice[j].t <= nt / 2 ) ? ( lattice[j].t ) : ( nt - lattice[j].t );
    return dist;
}
