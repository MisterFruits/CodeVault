#!/bin/bash
# if you are running on a system with queing facilities, 
# add appropriate headers here

#select location of benchmark source
SRCDIR=$PWD/../src

#select a temporary scratch directory. 
SCRATCHDIR=./scratch

#set up the scratch dir
rm -rf $SCRATCHDIR
mkdir -p $SCRATCHDIR
cd  $SCRATCHDIR

#copy input file
cp $SRCDIR/kernel_E.input .

#copy output reference files
cp -r $SRCDIR/output_ref .


#set the number of OpenMP threads (if using threaded version)
export OMP_NUM_THREADS=4

#run the code
$SRCDIR/bench



