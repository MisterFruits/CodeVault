.. UEABS for accelerators documentation master file, created by
   sphinx-quickstart on Wed Jun  7 19:01:00 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to UEABS for accelerators's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   4ip_extension


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
