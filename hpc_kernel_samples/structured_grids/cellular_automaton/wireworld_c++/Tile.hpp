#pragma once

#include <ostream>
#include <vector>

#include "Configuration.hpp"
#include "FileIO.hpp"
#include "MpiEnvironment.hpp"
#include "Util.hpp"

class Tile {
	const MpiEnvironment& env_;
	const Configuration& cfg_;

	const HeaderInfo header_;
	const Size tileSize_;
	const std::size_t modelWidth_;
	std::vector<State> memoryA_;
	std::vector<State> memoryB_;
	State* model_;
	State* nextModel_;

	Tile(const Configuration& cfg, const MpiEnvironment& env);

  public:
	auto& model() { return model_; }
	auto& model() const { return model_; }
	auto& modelWidth() const { return modelWidth_; }
	auto& nextModel() { return nextModel_; }
	auto tileSize() const { return tileSize_; }

	friend std::ostream& operator<<(std::ostream& out, const Tile& t);
	static Tile Read(const Configuration& cfg, const MpiEnvironment& env);
	void write() const;
};
