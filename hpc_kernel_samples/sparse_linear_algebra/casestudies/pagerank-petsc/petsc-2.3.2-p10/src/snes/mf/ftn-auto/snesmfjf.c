#include "petsc.h"
#include "petscfix.h"
/* snesmfj.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (*(long *)(a))
#define PetscFromPointer(a) (long)(a)
#define PetscRmPointer(a)
#endif

#include "petscsnes.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matcreatesnesmf_ MATCREATESNESMF
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matcreatesnesmf_ matcreatesnesmf
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfsetfromoptions_ MATSNESMFSETFROMOPTIONS
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfsetfromoptions_ matsnesmfsetfromoptions
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matcreatemf_ MATCREATEMF
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matcreatemf_ matcreatemf
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfgeth_ MATSNESMFGETH
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfgeth_ matsnesmfgeth
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfsetperiod_ MATSNESMFSETPERIOD
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfsetperiod_ matsnesmfsetperiod
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfsetfunctionerror_ MATSNESMFSETFUNCTIONERROR
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfsetfunctionerror_ matsnesmfsetfunctionerror
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfaddnullspace_ MATSNESMFADDNULLSPACE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfaddnullspace_ matsnesmfaddnullspace
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfsethhistory_ MATSNESMFSETHHISTORY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfsethhistory_ matsnesmfsethhistory
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfresethhistory_ MATSNESMFRESETHHISTORY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfresethhistory_ matsnesmfresethhistory
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfcomputejacobian_ MATSNESMFCOMPUTEJACOBIAN
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfcomputejacobian_ matsnesmfcomputejacobian
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfsetbase_ MATSNESMFSETBASE
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfsetbase_ matsnesmfsetbase
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define matsnesmfcheckpositivity_ MATSNESMFCHECKPOSITIVITY
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define matsnesmfcheckpositivity_ matsnesmfcheckpositivity
#endif


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
void PETSC_STDCALL   matcreatesnesmf_(SNES snes,Vec x,Mat *J, int *__ierr ){
*__ierr = MatCreateSNESMF(
	(SNES)PetscToPointer((snes) ),
	(Vec)PetscToPointer((x) ),J);
}
void PETSC_STDCALL   matsnesmfsetfromoptions_(Mat mat, int *__ierr ){
*__ierr = MatSNESMFSetFromOptions(
	(Mat)PetscToPointer((mat) ));
}
void PETSC_STDCALL   matcreatemf_(Vec x,Mat *J, int *__ierr ){
*__ierr = MatCreateMF(
	(Vec)PetscToPointer((x) ),J);
}
void PETSC_STDCALL   matsnesmfgeth_(Mat mat,PetscScalar *h, int *__ierr ){
*__ierr = MatSNESMFGetH(
	(Mat)PetscToPointer((mat) ),h);
}
void PETSC_STDCALL   matsnesmfsetperiod_(Mat mat,PetscInt *period, int *__ierr ){
*__ierr = MatSNESMFSetPeriod(
	(Mat)PetscToPointer((mat) ),*period);
}
void PETSC_STDCALL   matsnesmfsetfunctionerror_(Mat mat,PetscReal *error, int *__ierr ){
*__ierr = MatSNESMFSetFunctionError(
	(Mat)PetscToPointer((mat) ),*error);
}
void PETSC_STDCALL   matsnesmfaddnullspace_(Mat J,MatNullSpace nullsp, int *__ierr ){
*__ierr = MatSNESMFAddNullSpace(
	(Mat)PetscToPointer((J) ),
	(MatNullSpace)PetscToPointer((nullsp) ));
}
void PETSC_STDCALL   matsnesmfsethhistory_(Mat J,PetscScalar history[],PetscInt *nhistory, int *__ierr ){
*__ierr = MatSNESMFSetHHistory(
	(Mat)PetscToPointer((J) ),history,*nhistory);
}
void PETSC_STDCALL   matsnesmfresethhistory_(Mat J, int *__ierr ){
*__ierr = MatSNESMFResetHHistory(
	(Mat)PetscToPointer((J) ));
}
void PETSC_STDCALL   matsnesmfcomputejacobian_(SNES snes,Vec x,Mat *jac,Mat *B,MatStructure *flag,void*dummy, int *__ierr ){
*__ierr = MatSNESMFComputeJacobian(
	(SNES)PetscToPointer((snes) ),
	(Vec)PetscToPointer((x) ),jac,B,
	(MatStructure* )PetscToPointer((flag) ),dummy);
}
void PETSC_STDCALL   matsnesmfsetbase_(Mat J,Vec U, int *__ierr ){
*__ierr = MatSNESMFSetBase(
	(Mat)PetscToPointer((J) ),
	(Vec)PetscToPointer((U) ));
}
void PETSC_STDCALL   matsnesmfcheckpositivity_(Vec U,Vec a,PetscScalar *h,void*dummy, int *__ierr ){
*__ierr = MatSNESMFCheckPositivity(
	(Vec)PetscToPointer((U) ),
	(Vec)PetscToPointer((a) ),h,dummy);
}
#if defined(__cplusplus)
}
#endif
