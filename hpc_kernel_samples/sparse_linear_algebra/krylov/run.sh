mpirun -n 1 ./2_sparse_krylov_simple -ksp_monitor_short -m 5 -n 5 -ksp_gmres_cgs_refinement_type refine_always
mpirun -n 2 ./2_sparse_krylov_simple -ksp_monitor_short -m 5 -n 5 -mat_view draw -ksp_gmres_cgs_refinement_type refine_always -nox
mpirun -n 4 ./2_sparse_krylov_simple -pc_type bjacobi -pc_bjacobi_blocks 1 -ksp_monitor_short -sub_pc_type jacobi -sub_ksp_type gmres
mpirun -n 3 ./2_sparse_krylov_simple -ksp_type fbcgsr -pc_type bjacobi

mpirun -n 1 ./2_sparse_krylov_twosys -pc_type jacobi -ksp_monitor_short -ksp_gmres_cgs_refinement_type refine_always
mpirun -n 2 ./2_sparse_krylov_twosys -pc_type jacobi -ksp_monitor_short -ksp_gmres_cgs_refinement_type refine_always -ksp_rtol .000001
mpirun -n 2 ./2_sparse_krylov_twosys -ksp_gmres_cgs_refinement_type refine_always
mpirun -n 5 ./2_sparse_krylov_twosys -pc_type redundant -pc_redundant_number 1 -redundant_ksp_type gmres -redundant_pc_type jacobi -ksp_rtol 1.e-10

mpirun -n 2 ./2_sparse_krylov_multirhs -ntimes 4 -ksp_gmres_cgs_refinement_type refine_always
